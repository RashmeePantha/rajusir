<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('analyzing_factor', 'Analyzing_factorsController');
Route::resource('dataset', 'DatasetController');
oute::resource('job_type', 'Job_typeController');
oute::resource('personality_question', 'Personality_questionController');
oute::resource('question_options', 'Question_optionsController');
oute::resource('user_answers', 'User_answersController');
oute::resource('user_application', 'User_applicationController');
oute::resource('user_type', 'User_typeController');
oute::resource('user', 'UserController');
oute::resource('vacancy_requirement', 'Vacancy_requirementController');
oute::resource('vacancy', 'VacancyController');

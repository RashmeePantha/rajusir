<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_application extends Model
{
    Protected $guarded = [];

    public function user_answers()
    {
        return $this->hasMany(User_answers::class);
    }
}

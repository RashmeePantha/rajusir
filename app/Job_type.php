<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



class Job_type extends Model
{
    Protected $guarded = [];

   public function vacancies()
    {
        return $this->hasMany(Vacancies::class);
    } 
    public function personality_question()
    {
        return $this->hasMany(Personality_questions::class);
    } 
}

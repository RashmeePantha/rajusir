<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personality_questions extends Model
{
    public function question_options()
    {
        return $this->hasMany(Question_options::class);
    } 
}

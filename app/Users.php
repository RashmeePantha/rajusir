<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    public function user_application()
    {
        return $this->hasMany(User_application::class);
    } 
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vacancies extends Model
{
    Protected $guarded = [];

    public function vacancy_requirements()
    {
        return $this->hasMany(Vacancy_requirements::class);
    } 
}
